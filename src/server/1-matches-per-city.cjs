function matchesPerCity(matchesDetails) {
  if (arguments.length === 0) return {};

  if (!Array.isArray(matchesDetails)) return {};

  let matchPerCity = matchesDetails.reduce((acc, curr) => {
    let city = curr.City;
    if (city in acc) {
      acc[city] = acc[city] + 1;
    } else {
      acc[city] = 1;
    }
    return acc;
  }, {});

  return matchPerCity;
}

module.exports = matchesPerCity;