function bestTeam(winners){
    const points = {};
    points.Winner = 4;
    points["Runners-Up"] = 3;
    points.Third = 2;
    points.Fourth = 1;

    let teamPoints =  winners.reduce((acc, match) => {
        acc[match.Winner] = acc[match.Winner] || 0;
        acc[match.Winner] += points.Winner;

        acc[match["Runners-Up"]] = acc[match["Runners-Up"]] || 0;
        acc[match["Runners-Up"]] += points["Runners-Up"];

        acc[match.Third] = acc[match.Third] || 0;
        acc[match.Third] += points.Third;

        acc[match.Fourth] = acc[match.Fourth] || 0;
        acc[match.Fourth] += points.Fourth;
        return acc;
    }, {});

    teamPoints = Object.entries(teamPoints);
    teamPoints.sort((a, b) => b[1] - a[1]);
    teamPoints = Object.fromEntries(teamPoints);
    return teamPoints;
}

module.exports = bestTeam;