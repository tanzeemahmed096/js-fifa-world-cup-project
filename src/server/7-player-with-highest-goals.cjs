function playerWithHighestGoals(playersDetails){
    const filteredPlayersDetails = playersDetails.filter(player => player.Event !== "");

    let teamMate = "";
    let goals = 0;

    const playersGoals = filteredPlayersDetails.reduce((acc, player) => {
        let goalPerMatch = player.Event.split(" ").filter(event => event.charAt(0) === "G");
        // console.log(goalPerMatch);
        acc[player["Player Name"]] = acc[player["Player Name"]] || 0;
        acc[player["Player Name"]] += goalPerMatch.length;

        if(acc[player["Player Name"]] > goals){
            goals = acc[player["Player Name"]];
            teamMate = player["Player Name"];
        }
        return acc;
    }, {})

    let topPlayer = Object.entries(playersGoals);
    topPlayer.sort((a, b) => b[1] - a[1]);
    topPlayer = Object.fromEntries(topPlayer);

    return topPlayer;
    
}

module.exports = playerWithHighestGoals;