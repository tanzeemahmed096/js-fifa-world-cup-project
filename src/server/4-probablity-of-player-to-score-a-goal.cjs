function probabilityOfGoal(playersDetails) {
  if (arguments.length === 0) return {};

  if (!Array.isArray(playersDetails)) return {};

  let totalMatches = playersDetails.reduce((acc, curr) => {
    if (curr["Player Name"] in acc) {
      acc[curr["Player Name"]] += 1;
    } else {
      acc[curr["Player Name"]] = 1;
    }
    return acc;
  }, {});

  let playerGoals = playersDetails.reduce((acc, curr) => {
    let event = curr.Event;
    event = event.split(" ");
    let checkGoal = false;
    for (let value of event) {
      if (value.charAt(0) === "G") {
        checkGoal = true;
      }
    }

    if (checkGoal === true) {
      if (curr["Player Name"] in acc) {
        acc[curr["Player Name"]] += 1;
      } else {
        acc[curr["Player Name"]] = 1;
      }
    }
    return acc;
  }, {});

  let probabilityOfPlayer = Object.keys(playerGoals).reduce((acc, curr) => {
    acc[curr] = playerGoals[curr] / totalMatches[curr];
    return acc;
  }, {});

  let probabilityArr = Object.entries(probabilityOfPlayer);

  probabilityArr = probabilityArr
    .sort((a, b) => {
      return b[1] - a[1];
    })
    .slice(0, 10);

  const topTenPlayers = Object.fromEntries(probabilityArr);
  return topTenPlayers;
}

module.exports = probabilityOfGoal;