function groupStages(matchesDetails){
    const stages = matchesDetails.reduce((acc, match) => {
        acc[match.Stage] = 0;
        return acc;
    }, {});

    const groupStageTeam = matchesDetails.reduce((acc, match) => {
        acc[match["Home Team Name"]] = acc[match["Home Team Name"]] || {...stages};
        acc[match["Away Team Name"]] = acc[match["Away Team Name"]] || {...stages};

        acc[match["Home Team Name"]][match.Stage] += 1;
        acc[match["Away Team Name"]][match.Stage] += 1;
        return acc;
    }, {});

    return groupStageTeam;
}

module.exports = groupStages;