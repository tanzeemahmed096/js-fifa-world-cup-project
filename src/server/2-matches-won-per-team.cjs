function matchesWonPerTeam(matchesDetails) {
  if (arguments.length === 0) return {};

  if (!Array.isArray(matchesDetails)) return {};

  const homeTeam = "Home Team Name";
  const awayTeam = "Away Team Name";
  const homeTeamGoals = "Home Team Goals";
  const awayTeamGoals = "Away Team Goals";
  const winCondition = "Win conditions";

  let storeCountry = matchesDetails.reduce((acc, curr) => {
    const hTeam = curr[homeTeam];
    const aTeam = curr[awayTeam];
    if (hTeam in acc === false) {
      acc[hTeam] = 0;
    } else if (aTeam in acc === false) {
      acc[aTeam] = 0;
    }
    return acc;
  }, {});

  let matchWinPerTeam = matchesDetails.reduce((acc, curr) => {
    const hTeam = curr[homeTeam];
    const aTeam = curr[awayTeam];
    const win = curr[winCondition];

    if (curr[homeTeamGoals] > curr[awayTeamGoals]) {
      acc[hTeam] = acc[hTeam] + 1;
    } else if (curr[homeTeamGoals] < curr[awayTeamGoals]) {
      acc[aTeam] = acc[aTeam] + 1;
    } else {
      let penaltyWin = win.substring(win.indexOf("(") + 1, win.indexOf(")"));
      penaltyWin = penaltyWin.split(" ");
      if (penaltyWin[0] > penaltyWin[2]) {
        acc[hTeam] = acc[hTeam] + 1;
      } else if (penaltyWin[0] < penaltyWin[2]) {
        acc[aTeam] = acc[aTeam] + 1;
      }
    }

    return acc;
  }, storeCountry);

  return matchWinPerTeam;
}

module.exports = matchesWonPerTeam;
