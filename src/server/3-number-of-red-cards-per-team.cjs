function numOfRedCards(matchesDetails, playersDetails, year = 2014) {
  if (arguments.length === 0) return {};

  if (!Array.isArray(playersDetails)) return {};

  let numberOfRedCards = playersDetails.reduce((acc, curr) => {
    let event = curr.Event;
    event = event.split(" ");
    let checkCard = event.filter((value) => {
      return value.charAt(0) === "R";
    });

    if (checkCard.length > 0) {
      const teamName = penaltyTeamName(curr.MatchID, curr["Team Initials"]);
      if (teamName !== "") {
        if (teamName in acc) {
          acc[teamName] = acc[teamName] + 1;
        } else {
          acc[teamName] = 1;
        }
      }
    }

    return acc;
  }, {});

  function penaltyTeamName(MatchID, teamInitial) {
    const matchDetail = matchesDetails[MatchID];
    if (matchDetail[teamInitial] && matchDetail.Year === year.toString()) {
      return matchDetail[teamInitial];
    }

    return "";
  }

  return numberOfRedCards;
}

module.exports = numOfRedCards;