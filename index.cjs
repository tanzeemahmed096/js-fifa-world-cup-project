const csvtojson = require("csvtojson");
const fs = require("fs");
const path = require("path");

const csvMatchesDetails = path.join(__dirname, "src/data/WorldCupMatches.csv");
const csvPlayersDetails = path.join(__dirname, "src/data/WorldCupPlayers.csv");
const csvWorldCups = path.join(__dirname, "./src/data/WorldCups.csv");

// 1. Matches per city
const matchesPerCity = require("./src/server/1-matches-per-city.cjs");
csvtojson()
  .fromFile(csvMatchesDetails)
  .then((jsonObj) => {
    let result = matchesPerCity(filteredData(jsonObj));
    fs.writeFileSync(
      path.join(__dirname, "./src/public/output/1-matches-per-city.json"),
      JSON.stringify(result, null, 2),
      "utf-8"
    );
  });

// 2. Matches won per team
const matchesWonPerTeam = require("./src/server/2-matches-won-per-team.cjs");
csvtojson()
  .fromFile(csvMatchesDetails)
  .then((jsonObj) => {
    let result = matchesWonPerTeam(filteredData(jsonObj));
    fs.writeFileSync(
      path.join(__dirname, "./src/public/output/2-matches-won-per-team.json"),
      JSON.stringify(result, null, 2),
      "utf-8"
    );
  });

//3. Number of red cards per team
const numOfRedCards = require("./src/server/3-number-of-red-cards-per-team.cjs");
csvtojson()
  .fromFile(csvMatchesDetails)
  .then((jsonObjMatches) => {
    csvtojson()
      .fromFile(csvPlayersDetails)
      .then((jsonObjPlayers) => {
        const teamsDetails = jsonObjMatches.filter(
          (matches) => matches.Year !== ""
        );

        const teamDetails = teamsDetails.reduce((acc, curr) => {
          acc[curr.MatchID] = {};
          acc[curr.MatchID].Year = curr.Year;
          acc[curr.MatchID][curr["Home Team Initials"]] = curr["Home Team Name"];
          acc[curr.MatchID][curr["Away Team Initials"]] = curr["Away Team Name"];
          return acc;
        }, {});
        // console.log(teamDetails);

        let result = numOfRedCards(teamDetails, jsonObjPlayers, 2014);
        fs.writeFileSync(
          path.join(
            __dirname,
            "./src/public/output/3-number-of-red-cards-per-team.json"
          ),
          JSON.stringify(result, null, 2),
          "utf-8"
        );
      });
  });

// 4. Top 10 players with the highest probability of scoring a goal in a match
const probabilityOfGoal = require("./src/server/4-probablity-of-player-to-score-a-goal.cjs");
csvtojson()
  .fromFile(csvPlayersDetails)
  .then((jsonObj) => {
    let result = probabilityOfGoal(jsonObj);
    fs.writeFileSync(
      path.join(
        __dirname,
        "./src/public/output/4-probablity-of-player-to-score-a-goal.json"
      ),
      JSON.stringify(result, null, 2),
      "utf-8"
    );
  });

  function filteredData(obj){
    return obj.filter(element => element.City !== "");
  }


// Best team in world cup
const bestTeam = require("./src/server/5-best-team.cjs");
csvtojson()
  .fromFile(csvWorldCups)
  .then((jsonObj) => {
    let result = bestTeam(jsonObj);
    fs.writeFileSync(
      path.join(
        __dirname,
        "./src/public/output/5-best-team.json"
      ),
      JSON.stringify(result, null, 2),
      "utf-8"
    );
});

// Matches played per staged per team
const groupStages = require("./src/server/6-group-stage-matches.cjs");
csvtojson()
  .fromFile(csvMatchesDetails)
  .then((jsonObj) => {
    const filterData = filteredData(jsonObj);
    let result = groupStages(filterData);
    fs.writeFileSync(
      path.join(
        __dirname,
        "./src/public/output/6-group-stage-matches.json"
      ),
      JSON.stringify(result, null, 2),
      "utf-8"
    );
});

//Player with highest number of goals
const playerWithHighestGoals = require("./src/server/7-player-with-highest-goals.cjs");
csvtojson()
  .fromFile(csvPlayersDetails)
  .then((jsonObj) => {
    let result = playerWithHighestGoals(jsonObj);
    fs.writeFileSync(
      path.join(
        __dirname,
        "./src/public/output/7-player-with-highest-goals.json"
      ),
      JSON.stringify(result, null, 2),
      "utf-8"
    );
});